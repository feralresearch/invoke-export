const sqlite3 = require("sqlite3").verbose();
const fs = require("fs");

///////

const inputPath = "D:/AI/InvokeAI"
const outputDir = "./OUTPUT";
const filterOnKeys = []; //"positive_prompt", "negative_prompt"
const gatherFiles = false;

//////

const dataPath = `${inputPath}/databases/invokeai.db`;
const imgPath = `${inputPath}/outputs/images`;
if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir, { recursive: true });

let db = new sqlite3.Database(dataPath, (err) => {
    if (err) {
        console.error(dataPath);
        console.error(err.message);
    }
});

let fileCount = 1;
db.serialize(() => {
    db.each(`SELECT * FROM images`, (err, row, count) => {
        fileCount++;
        if (err) {
            console.error(err.message);
        } else {
            const metadata = JSON.parse(row.metadata);
            const fileName = row.image_name.split(".png")[0];
            if (metadata) {
                let outputData = {};
                if (filterOnKeys.length > 0) {
                    Object.keys(metadata).forEach((key) => {
                        if (filterOnKeys.includes(key))
                            outputData[key] = metadata[key]
                    })
                } else {
                    outputData = metadata;
                }

                if (gatherFiles) {
                    const outputFolder = `${outputDir}/${fileName}`
                    if (!fs.existsSync(outputFolder)) fs.mkdirSync(outputFolder, { recursive: true });

                    const inImage = fs.createReadStream(`${imgPath}/${fileName}.png`);
                    const outImage = fs.createWriteStream(`${outputDir}/${fileName}/image.png`);

                    const inThumb = fs.createReadStream(`${imgPath}/thumbnails/${fileName}.webp`);
                    const outThumb = fs.createWriteStream(`${outputDir}/${fileName}/thumb.webp`);

                    inImage.pipe(outImage);
                    inThumb.pipe(outThumb);

                    fs.writeFile(
                        `${outputFolder}/metadata.json`,
                        JSON.stringify(outputData, null, 4),
                        (err) => {
                            if (err) console.error(err);
                        },
                    );

                } else {
                    fs.writeFile(
                        `${outputDir}/${fileName}.json`,
                        JSON.stringify(outputData, null, 4),
                        (err) => {
                            if (err) console.error(err);
                        },
                    );
                }

            } else {
                console.warn(`- WARN: ${fileName} missing metadata`);
            }
        }
    });
});

db.close((err) => {
    if (err) {
        console.error(err.message);
    } else {
        console.log(`Parsed ${fileCount} files`);
    }
});
